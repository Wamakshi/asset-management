﻿using System;
using System.Collections.Generic;
using AssetManagement.Models;
using AssetManagement.Controllers;
using AssetManagement.Shared;

namespace AssetManagement
{
    class Program
    {
        public static List<Employee> employees = new List<Employee>();
        public static List<Asset> assets = new List<Asset>();
        public static List<AssetAllocation> assetAllocationList = new List<AssetAllocation>();
        static AssetController assetController = new AssetController();
        static EmployeeController employeeController;
        static Employee user;
        static Menu menu = new Menu(assetController);        
        static void Main(string[] args)
        {
            string userExit = "n";
            Utilities.GenerateData();
            while(userExit.Equals(("n"))) {
                user = Utilities.LoginUser();
                if(!(user is null)) {
                    employeeController = new EmployeeController(user, assetController); 
                    if(user.type.Equals("user")) {
                        menu.handleEmployee(employeeController);
                    } else {
                        menu.handleAdmin();
                    }
                } else {
                    System.Console.WriteLine("Invalid User! Please try again");
                }  
                System.Console.WriteLine("Enter as new user? (y) or (n)");
                userExit = Console.ReadLine(); 
            }
           
        } 
    }
}
