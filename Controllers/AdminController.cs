using System;
using AssetManagement.Models;
using AssetManagement.Controllers;
using System.Linq;

namespace AssetManagement.Controllers
{
    public class AdminController
    {
        public void addAsset() {
            System.Console.WriteLine("Select Category - Monitors (1) or Books (2)");
            int categoryId =  int.Parse(Console.ReadLine());
            System.Console.WriteLine("Enter name of the asset:");
            string name = Console.ReadLine();
            System.Console.WriteLine("Enter asset description:");
            string desc = Console.ReadLine();
            System.Console.WriteLine("Enter amount:");
            int amount = int.Parse(Console.ReadLine()); 
            int id = Program.assets.Count > 0 ? Program.assets.Max(x => x.id) + 1 : 1;
            Program.assets.Add(new Asset(id, name, categoryId, Status.Unassigned, desc, amount));
            System.Console.WriteLine("Asset added successfully.");
        }

        public void delete(int assetId) {
            try {
                var itemToRemove = Program.assets.Single(item => item.id == assetId);
                Program.assets.Remove(itemToRemove);
                System.Console.WriteLine("Item deleted successfully");
            } catch(Exception e) {
                System.Console.WriteLine("Some error occured. Please try again later.");
                System.Console.WriteLine(e.Message);
            }
        }

        public void assign(int assetId) {
           
        }

        public void viewAllocationRequests() {
            System.Console.WriteLine("Employee ID \tAsset Requested \t Asset Status \t Request Pending");
            Program.assetAllocationList.ForEach(request => {
                Asset asset = Program.assets.Find(item => item.id.Equals(request.assetId));
                System.Console.WriteLine(request.empID+"\t"+asset.name+"\t"+asset.status+"\t"+request.isPendingRequest);
            });
        }

    }
}