using AssetManagement.Models;
using System.Collections.Generic;

namespace AssetManagement.Controllers
{
    public class AssetController
    {
        public void assign(Asset asset, int employeeId) {
            System.Console.WriteLine(asset.status);
            if(asset.status.Equals(Status.Unavailable)) {
                System.Console.WriteLine("Asset is unavailable");
            } else if (asset.status.Equals(Status.Assigned)) {
                System.Console.WriteLine("Asset is already assigned");
            } else {
                asset.status = Status.Assigned;
                asset.assignedTo = employeeId;
                System.Console.WriteLine("Assigned successfully");
            }
        }

        public void unassign(int assetId, int employeeId) {
            Asset asset = Program.assets.Find(x => x.id.Equals(assetId));
            asset.status = Status.Unassigned;
            asset.assignedTo = 0;
            System.Console.WriteLine("Unassigned successfully");
        }

        public List<Asset> findAssetsByCategory(List<Asset> assets, int category) {
            return assets.FindAll(x => x.categoryId.Equals(category));
        }

        public void showAssetDetails(int categoryId) {
            List<Asset> selectedCatAssets = Program.assets.FindAll(x => x.categoryId.Equals(categoryId));
            
            selectedCatAssets.ForEach((Asset asset) => {
            System.Console.Write("ID: {0}\nName: {1}\nStatus: {2}\nDescription: {3}\n", asset.id, asset.name, asset.status, asset.description);
            System.Console.WriteLine("------------------------");
            });
        }
    }

}