using AssetManagement.Models;
using System.Collections.Generic;

namespace AssetManagement.Controllers
{
    public class EmployeeController
    {
        private Employee employee;
        private AssetController assetController;

        public EmployeeController(Employee employee, AssetController assetController) {
            this.employee = employee;
            this.assetController = assetController;
        }

        public void requestAsset(int assetId) {
            Program.assetAllocationList.Add(new AssetAllocation(this.employee.empId, assetId, true));
            System.Console.WriteLine("Request has been sent for approval.");         
        }

        public void unassignAsset(int assetId) {
            this.assetController.unassign(assetId, this.employee.empId);    
        }
    }
}