Functionality

* Take user credential
    - Identify type of User
* Show menu for User
    - Select Category
        - Select asset
          - View
          - Assign Request
          - Unassign Request
    - Show my allocation history
* Additional options for Admin
    - Show all history
    - Asset Management
        - Create
        - Edit
        - delete
    - Allocation Requests
