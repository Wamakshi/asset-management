using System;
using AssetManagement.Models;
using AssetManagement.Controllers;

namespace AssetManagement.Shared
{
    public class Utilities
    {
        public static void GenerateData() {
            Program.employees.Add(new Employee(1, "admin", "1234", "admin@gmail.com", "admin"));
            Program.employees.Add(new Employee(1, "user", "1234", "user@gmail.com", "user"));
            Program.assets.Add(new Asset(1,"Monitor 1", 1,Status.Unassigned, "Dell Monitor 21 inch",  11000));
            Program.assets.Add(new Asset(2,"monitor 2", 1,Status.Assigned, "LG Monitor 21 inch",  12000));
            Program.assets.Add(new Asset(3,"Book 1", 2,Status.Unassigned, "The Magic of thinking Big",  11000));
            Program.assets.Add(new Asset(4,"Book 2", 2,Status.Assigned, "Untethered Soul",  12000));
        }

         static public Employee LoginUser() {
            Console.WriteLine("Hi Welcome to Asset Management !");
            System.Console.WriteLine("Enter Email:");
            string email = Console.ReadLine();
            System.Console.WriteLine("Enter Password:");
            string password = Console.ReadLine();
            return Program.employees.Find(emp => 
                 emp.email.Equals(email) && emp.password.Equals(password)
            );
        }
    }    
}