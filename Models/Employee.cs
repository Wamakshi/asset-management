namespace AssetManagement.Models
{
    public class Employee
    {
        public Employee(int empId, string name, string password, string email, string type) {
            this.empId = empId;
            this.name = name;
            this.password = password;
            this.email = email;
            this.type = type;
        }
        public int empId { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string type { get; set; }

    }
}