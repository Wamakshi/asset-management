namespace AssetManagement.Models
{
    public class Asset
    {
        public Asset(int id, string name, int categoryId, Status status, string desc, int amount) {
            this.id = id;
            this.name = name;
            this.categoryId = categoryId;
            this.status = status;
            this.description = desc;
            this.amount = amount;
        }
          public int id { get; set; }
          public string name { get; set; }
          public int categoryId { get; set; }
          public Status status { get; set; }
          public int assignedTo { get; set; }
          public string description { get; set; }
          int amount { get; set; }

    }
}