namespace AssetManagement.Models
{
    public class AssetAllocation
    {
        public AssetAllocation(int empId, int assetId, bool isPendingRequest) {
            this.empID = empID;
            this.assetId = assetId;
            this.isPendingRequest = isPendingRequest;
        }
        public int empID { get; set; }
        public int assetId  { get; set; }
        public bool isPendingRequest { get; set; }
        public int allocationId { get; set; }
        public string assignedOn { get; set; }
        public string unassignedOn { get; set; }
    }
}