namespace AssetManagement.Models
{
    public enum Status
    {
        Unavailable,
        Assigned,
        Unassigned
    }
}