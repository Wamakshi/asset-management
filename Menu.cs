using System;
using System.Collections.Generic;
using AssetManagement.Models;
using AssetManagement.Controllers;
using AssetManagement.Shared;

namespace AssetManagement
{
    public class Menu
    {
        EmployeeController employeeController;
        AdminController adminController = new AdminController();
        AssetController assetController;
        string userExit = "n";

        public Menu(AssetController assetController) {
            this.assetController = assetController;
        }
        #region Employee Menu
         public void handleEmployee(EmployeeController employeeController) {
            System.Console.WriteLine("*********Employee Menu**********");
            
            while(this.userExit.Equals(("n"))) {
                this.employeeController = employeeController;

                int category = getAssetCategory();
            
                assetController.showAssetDetails(category);       
        
                string option = getAssetOperationOption();
                performAssetOperation(option);
                
                getUserExitInput();
            }
           
        }

        private string getAssetOperationOption() {
            System.Console.WriteLine("What do you want to do ?");
            System.Console.WriteLine("1. Request to Assign asset\n2. Request to Unassign Asset");
            return Console.ReadLine();
        }
        
        private void performAssetOperation(string option) {
            int selectId = getAssetId();

            switch(option) {

                case "1":
                employeeController.requestAsset(selectId);                       
                break;

                case "2":
                employeeController.unassignAsset(selectId);     
                break;

                default:
                System.Console.WriteLine("Invalid Option");
                break;
            }
        }
        #endregion

        #region Admin Menu

        public void handleAdmin() {
            System.Console.WriteLine("*********Admin Menu**********");
            
            int option = getAdminMenuOption();
             switch(option) {
                case 1:
                this.showAssetManagementMenu();
                break;
                case 2:
                this.showAssetAllocationMenu();
                break;
                default:
                System.Console.WriteLine("Invalid Option");
                break;
             }
        }

        private int getAdminMenuOption() {
            System.Console.WriteLine("1. Asset Management.\n2. Allocation Requests");
            return int.Parse(Console.ReadLine());
        }

        private void showAssetAllocationMenu() {
             this.adminController.viewAllocationRequests();
             System.Console.WriteLine("To approve a request - ");
             int assetId = getAssetId();
             this.adminController.assign(assetId);
        }

        private void showAssetManagementMenu() {
            System.Console.WriteLine("1. Add new asset\n2. Delete an asset");
            int assetManageOption =  int.Parse(Console.ReadLine());

            switch(assetManageOption) {
                case 1:
                this.adminController.addAsset();
                break;
                case 2:
                int categoryId = getAssetCategory();
                this.assetController.showAssetDetails(categoryId);               
                this.adminController.delete(this.getAssetId());
                break;
                default:
                System.Console.WriteLine("Invalid Option");
                break;

            }
        }

        #endregion

        private int getAssetId() {
            System.Console.WriteLine("Select Asset ID:");
            return int.Parse(Console.ReadLine());
        }
         private int getAssetCategory() {
            System.Console.WriteLine("Select Asset Category :");
            System.Console.WriteLine("1. Monitors \n2. Books");
            return int.Parse(Console.ReadLine());  
        }

        public void getUserExitInput() {
            System.Console.WriteLine("Do you want to exit? (y) or (n)?");
            this.userExit = Console.ReadLine();
        }
    }
}